package additional.task.one;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TextSaver {
    private TextTransformer textTransformer;
    private File file;

    public TextSaver() {
        super();
    }

    public TextSaver(TextTransformer textTransformer, File file) {
        super();
        this.textTransformer = textTransformer;
        this.file = file;
    }

    public TextTransformer getTextTransformer() {
        return textTransformer;
    }

    public void setTextTransformer(TextTransformer textTransformer) {
        this.textTransformer = textTransformer;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void saveTextToFile(String text) {
        try (PrintWriter printWriter = new PrintWriter(this.file)) {
            printWriter.println(this.textTransformer.transform(text));
            System.out.println("Text saved to file.");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
