package additional.task.one;

public class TextTransformer {
    public TextTransformer() {
        super();
    }

    public String transform(String text) {
        return text.toUpperCase();
    }
}
