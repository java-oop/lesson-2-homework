package additional.task.one;

public class UpDownTransformer extends TextTransformer {
    public UpDownTransformer() {
        super();
    }

    @Override
    public String transform(String text) {
        char[] symbols = text.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < symbols.length; i += 1) {
            stringBuilder.append(i % 2 == 0 ? Character.toUpperCase(symbols[i]) : Character.toLowerCase(symbols[i]));
        }

        return stringBuilder.toString();
    }
}
