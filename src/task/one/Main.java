/*
1) Создать класс Animal.
У него должны быть поля:
String ration (чем питается животное)
String color (какого оно цвета)
int weight (вес)
Методы:
Стандартные (методы получения и установки, toString() и т. д.)
String getVoice() (подать голос)
void eat() (есть)
void sleep() (спать)
2) Создать классы Cat, Dog как подклассы Animal. Добавьте новое поле String name.
Переопределите методы getVoice(), eat(), sleep().
3) Создайте класс Veterinarian. Поля:
String name
Методы:
Стандартные
void treatment(Animal animal) (Лечение животного. Лечить можно и кошек и собак)
 */

package task.one;

public class Main {
    public static void main(String[] args) {
        Veterinarian veterinarian = new Veterinarian("Oleg");
        Cat cat = new Cat("Cat`s food", "black", 5, "Simka");
        Dog dog = new Dog("Meat", "white", 15, "Pedro");

        System.out.println(cat.getVoice());
        cat.eat();
        cat.sleep();

        System.out.println(dog.getVoice());
        dog.eat();
        dog.sleep();

        cat.getSick();
        dog.getSick();

        System.out.println(veterinarian);
        veterinarian.treatment(cat);
        veterinarian.treatment(dog);
    }
}
