package task.one;

public class Cat extends Animal {
    private String name;

    public Cat() {
        super();
    }

    public Cat(String ration, String color, int weight, String name) {
        super(ration, color, weight);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getVoice() {
        return "Meow meow meow";
    }

    @Override
    public void sleep() {
        System.out.println("Sleeping like a cat...");
    }

    @Override
    public void eat() {
        System.out.println("Eating cat`s food...");
    }

    @Override
    public String toString() {
        return "Cat{" +
                super.toString() +
                ", name='" + name + '\'' +
                '}';
    }
}
