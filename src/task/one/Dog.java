package task.one;

public class Dog extends Animal {
    private String name;

    public Dog() {
        super();
    }

    public Dog(String ration, String color, int weight, String name) {
        super(ration, color, weight);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getVoice() {
        return "GAV GAV GAV";
    }

    @Override
    public void sleep() {
        System.out.println("Sleeping like a dog...");
    }

    @Override
    public void eat() {
        System.out.println("Eating dog`s food...");
    }

    @Override
    public String toString() {
        return "Dog{" +
                super.toString() +
                ", name='" + name + '\'' +
                '}';
    }
}
