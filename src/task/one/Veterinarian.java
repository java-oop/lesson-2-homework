package task.one;

public class Veterinarian {
    private String name;

    public Veterinarian() {
        super();
    }

    public Veterinarian(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void treatment(Animal animal) {
        animal.setHealth(100);
        System.out.println("Treating " + animal);
    }

    @Override
    public String toString() {
        return "Veterinarian{" +
                "name='" + name + '\'' +
                '}';
    }
}
